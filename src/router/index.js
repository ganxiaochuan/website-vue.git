import Vue from 'vue'
import Router from 'vue-router'

import index from '@/page/index' //首页
import page from '@/page/page' //新闻详情页
import newsList from '@/page/newsList' //新闻列表
import productList from '@/page/productList' //产品列表
import product from '@/page/product' //产品详情
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: index
    },
    {
      path: '/page/:id/:active',
      component: page
    },
    {
      path: '/newsList/:id/:page/:active',
      component: newsList
    },
    {
      path: '/productList/:id/:page/:active',
      component: productList
    },
    {
      path: '/product/:id/:active',
      component: product
    },
  ]
})
